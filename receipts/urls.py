from django.urls import path
from .views import receipts_view

urlpatterns = [path("", receipts_view, name="home")]
