from django.shortcuts import render, redirect
from .models import Receipt


# Create your views here.
def receipts_view(request):
    receipt = Receipt.objects.all()
    context = {
        "receipt": receipt,
    }
    return render(request, "receipt.html", context)


def redirect_home(request):
    return redirect("home")
